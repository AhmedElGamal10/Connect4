#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <ctype.h>
#define true 1
#define false 0


/*
declare victory
*/

int iT=0;  /// Pointer to current game  0 < iT < nT
int nT=0;  /// indicates the total turns played
int flaggedRow;  /// indicates last affected row
int TurnCol[1000]={0};
char gameArr[100][100]={{0}};
int loading = false;


struct conf{       ///General game settings
    int Rows;
    int Cols;
    int GameMode;
    int GameDifficulty;
    int Highscores;
    int scoreA;
    int scoreB;
}Game;

struct AI{
    int col;  /// Selected Column
    int scoreX;  ///Selected Score for Human
    int scoreO;  ///Selected Score for CPU
    int level;   ///Furthest level where X score increases
    int currParCol;  ///Current Root Column (cell)
    int curr_cell_scoreX; /// Total Root Score for X
    int curr_cell_scoreO; /// For O
    int curr_cell_level; /// closest level Where x score increases
};
int check_end(){
    int i;
    for(i=0 ; i < Game.Cols ; i++){
        if(gameArr[Game.Rows - 1][i] == 0 )
            return 0;
    }
    if(nT > 0)
        return 1;
    else
        return 0;
}
int xml_parser(char FileName[],char Tag[],char parentTag[20]){
    char filename[20];
    strcpy(filename,FileName);  /// copy the string into filename as string pointer isn't supported in strcat
    strcat(filename,".xml"); /// adds xml extension

    FILE*input=fopen(filename,"r");

    int inside_parent=false, inside_tag=false , stop=false;
    int value=-1;
    int position;
    char st[100];

    char OpenParentTag[25];
    strcpy(OpenParentTag,"<");
    strcat(OpenParentTag,parentTag); /// creates a new string with <, ex tag -> <tag to indicate tag's start and end
    strcat(OpenParentTag,">"); /// same as above

    char closeParentTag[25];
    strcpy(closeParentTag,"</");
    strcat(closeParentTag,parentTag);
    strcat(closeParentTag,">"); /// same as above

    char closeTag[25];
    strcpy(closeTag,"</");
    strcat(closeTag,Tag); /// same as above
    strcat(closeTag,">"); /// same as above

    char openTag[25];
    strcpy(openTag,"<");
    strcat(openTag,Tag); /// same as above
    strcat(openTag,">"); /// same as above


    if(input == NULL)   /// Checks if the file exists
        return -1;

    while(fgets(st, sizeof st, input) != NULL && !stop){  /// puts each line of the file in string st
        if(strstr(st,OpenParentTag))  /// toggle true at line where the parent tag is opened
            inside_parent=true;
        if(strstr(st,closeParentTag)){ /// toggle false at line where the parent tag is closed
            inside_parent=false;
            stop=true;  /// Stop to stop the any needless readings
        }
        if(strstr(st,openTag) && inside_parent ){   /// checks if tag is opened here
//            position=strstr(st,">");
//            sscanf(position+1,"%d</",&value);  /// scans its value
            inside_tag = true;
        }
        if(inside_tag && inside_parent){
            int i;
            for(i=0 ; st[i] != '\n' ; i++){
                if(st[i] >= '0' && st[i] <= '9'){
                    sscanf(st+i,"%d",&value);
                    break;
                }
            }
            if(strstr(st,closeTag))
                inside_tag = false;
        }

    }
    if(inside_parent || inside_tag)
        value = -1;
    fclose(input);  ///closes the connection with the file
    return value;
}
void clear_game(){ /// Used to reset game array and counters
    int i=0;
    for(i=0;i<nT;i++){
//        Turn[i].col=0;
        TurnCol[i]=0;
        /*Turn[i].scoreA=0;
        Turn[i].scoreB=0;*/
    }

    int i2=0;
    for(i=0;i<Game.Rows;i++){  ///resets the game array
        for(i2=0;i2<Game.Cols;i2++)
            gameArr[i][i2]=0;
    }
    /*memset(Game.PlayerNameA,0,sizeof(Game.PlayerNameA)); /// used to clear string
    memset(Game.PlayerNameB,0,sizeof(Game.PlayerNameB));*/
    iT=0;nT=0;Game.scoreA=0;Game.scoreB=0;
}
void load_defaults(){
    ///Width = 7, Height = 6, Highscores = 5
    Game.Cols=7;
    Game.Rows=6;
    Game.Highscores=5;

    FILE*config=fopen("conf.xml","w");
    fprintf(config,"\t<Configurations>\n\t\t<Height> 6 </Height>\n\t\t<Width> 7 </Width>\n\t\t<HighScores> 5 </HighScores>\n\t</Configurations>");
    fclose(config);
}
void define_game(){ ///used to read game mode, player names and define configurations from conf file

    int toggle=true;   /// used in loops to check the input till it is written correctly

    clear_game();
    system("cls");  /// clear the screen top prevent the showing of unrelated stuff
    int rows, cols , highscores;
    rows=xml_parser("conf","Height","Configurations"); /// Read configurations using xml parser
    cols=xml_parser("conf","Width","Configurations");
    highscores=xml_parser("conf","HighScores","Configurations");
    if((int)rows != rows || (int)cols != cols || (int)highscores != highscores || rows < 4 || cols < 4 || rows > 40 || cols > 40 || highscores < 1 || highscores > 50){
        printf("\n\n\nConfiguration Error ! Check if the file has been edited or deleted !\n\n\nDo you want to load the defaults ? ? Yes[y], No[n]");
        char choice[20]={0};
        scanf("%s",&choice);
        (choice[0]=='y'||choice[0]=='Y')?load_defaults():start_game();
    }else{
        Game.Rows=rows;  /// Save configurations
        Game.Cols=cols;
        Game.Highscores=highscores;
    }
    system("cls");

    char gmode[20]={0};
    while(toggle){
        toggle = false;
        printf("\n Choose game mode : \n\tSingle Player [1]\n\tTwo Players [2] \n\t ");
        scanf("%s",&gmode);
        if(atoi(gmode) == 1){
            Game.GameMode = 1;
        }else if(atoi(gmode) == 2){
            Game.GameMode = 2;
        }else{
            system ("cls");
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FORMATDLGORD30);
            printf("\n Wrong input, Please try again\n");
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FORMATDLGORD31);
            toggle = true;
        }
    }

}
void PrintHighscores (){

    int highscores=xml_parser("conf","HighScores","Configurations");
    if((int)highscores != highscores || highscores < 1 || highscores > 50){
        printf("\n\n\nConfiguration Error ! Check if the file has been edited or deleted !\n\n\nDo you want to load the defaults ? ? Yes[y], No[n]");
        char choice[20]={0};
        scanf("%s",&choice);
        (choice[0]=='y'||choice[0]=='Y')?load_defaults():start_game();
    }else{
        Game.Highscores=highscores;
    }
    int HighScores[50]={0};
    FILE*LoadHS = fopen("Highscore.txt","r");
    int i=0;
    while(true){
        fscanf(LoadHS,"%d",&HighScores[i]);
        if (HighScores[i]==NULL || HighScores[i]==0){      ///read all stored scores from txt file
            fclose(LoadHS);
            break;
        }
        i++;
    }
    if( check_end()){ /// Indicates That the game has ended to check new scores
        i=0;
        while(HighScores[i] != 0){
            i++;
        }
        HighScores[i] = Game.scoreA;
        if( Game.GameMode == 2)
            HighScores[i+1] = Game.scoreB;
        GotoXY(30,3);
        SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),BACKGROUND_GREEN | FORMATDLGORD31);
        printf("GAME HAS ENDED");
        GotoXY(35,4);
        if(Game.GameMode == 1 && Game.scoreA > Game.scoreB){
            printf("PLAYER WINS");
        }else if(Game.GameMode == 1 && Game.scoreB > Game.scoreA){
            printf("CPU WINS");
        }else if(Game.GameMode == 1 && Game.scoreB == Game.scoreA){
            printf("DRAW");
        }else if(Game.scoreA > Game.scoreB){
            printf("PLAYER ONE WINS");
        }else if(Game.scoreB > Game.scoreA){
            printf("PLAYER TWO WINS");
        }else{
            printf("DRAW");
        }
        GotoXY(32,6);
        printf("X : %d",Game.scoreA);
        GotoXY(32,7);
        printf("O : %d",Game.scoreB);
        clear_game();
    }
    int k,m;
    for (k=0;HighScores[k] != 0;k++){             ///Sort the read numbers in a descending order
        for (m=0;HighScores[m] != 0;m++){
            if (HighScores[m]<HighScores[m+1]){         ///it doesn't matter much whether HighSores Would be of size area or bigger
                int temp=HighScores[m];
                HighScores[m]=HighScores[m+1];
                HighScores[m+1]=temp;
            }
        }
    }
    FILE*SaveHS = fopen("Highscore.txt","w");
    for (i=0;HighScores[i] != 0;i++){
       fprintf(SaveHS," %d",HighScores[i]);
    }
    fclose(SaveHS);
    GotoXY(60,1);
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_GREEN | FOREGROUND_RED);
    printf("HighScores");
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FORMATDLGORD31);
    for (i=0;i<Game.Highscores;i++){
        GotoXY(64,i+3);
        printf("%d",HighScores[i]);
    }
    GotoXY(0,2);

}
void start_game(){ /// called to start the game
    system("cls");
    PrintHighscores();
    printf("\n1. New Game\n2. Load Game\n3. Exit");
    if(nT>0 && nT < Game.Cols * Game.Rows)
        printf("\n4. Continue");
    printf("\n\nChoice : ");
    char choice[20]={0};
    scanf("%s",&choice);
    if(nT>0 && nT < Game.Cols * Game.Rows)
        atoi(choice)==4?print_game():1;
    switch (atoi(choice)){
        case 1:
            define_game();
            print_game(-1);
            break;
        case 2:
            load();
            break;
        case 3:
            system("cls");
            puts("\n\n\n\n Thanks for playing ^_^");
            exit(0);
            break;
        default:
            printf("Invalid Request !!\n");
            start_game();
    }
}
void GotoXY(int x,int y){
 COORD coord;
 coord.X=x;
 coord.Y=y;
 SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),coord);
}

void print_game(int error){  ///print the current board
    check_score();
    int i,i2;
    int tempI;
    system("cls");
    printf("\n\n\n\n"); /// Lowering output
    if(error == 0){
        tempI=gameArr[flaggedRow][TurnCol[iT-1]];
        gameArr[flaggedRow][TurnCol[iT-1]]=0;
    }
    for(i=Game.Rows-1;i>=0;i--){
        printf("\t");
        for(i2=0;i2<Game.Cols;i2++){
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_BLUE); /// Changing text color
            printf("|");
            if(gameArr[i][i2]==1){
                SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN);
                printf(" X ");
            }else if (gameArr[i][i2]==2){
                SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_GREEN);
                printf(" O ");
            }else{
                SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_BLUE);
                printf("   ");
            }
        }
        SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_BLUE);
        printf("|\n\t");
        for(i2=0;i2<Game.Cols;i2++){
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_BLUE | FOREGROUND_INTENSITY);
            printf("|---"); /// Put a row of  |---|---|---| under each row
        }
        printf("|"); /// Close the Right side

        if(i == (int)Game.Rows/2+1){
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN);
            printf("\tPlayer One : %d",Game.scoreA);
        }
        if(i == (int)Game.Rows/2){
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_GREEN);
            if(Game.GameMode == 2)
                printf("\tPlayer Two : %d",Game.scoreB);
            else
                printf("\tCPU : %d",Game.scoreB);
        }
        printf("\n");
    }
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_BLUE);
    printf("\t ");
    for(i=1;i <= Game.Cols ; i++)
        i<10?printf(" %d  ",i):printf(" %d ",i);

    /// Print Errors:
    error > 0?SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED):1; /// change text color to red if there are any errors
    if(error==1)
        printf("\n\n\n\n\nInvalid Input \n");
    else if(error == 2)
        printf("\n\n\n\n\nColumn Full \n");
    else if(error == 3)
        printf("\n\n\n\n\nThis is the First Move \n");
    else if(error == 4)
        printf("\n\n\n\n\nThis is the last Move \n");
    else if(error == 0){  /// Animation
        gameArr[flaggedRow][TurnCol[iT-1]]=tempI;
        for(i=0;i <4 ;i++){
            GotoXY(13+4*(TurnCol[iT-1]-1),i);
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FORMATDLGORD31);
            iT%2==0?printf(" O "):printf(" X ");
            Sleep(20); /// Delays the output with *ms giving the animation effect
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FORMATDLGORD31);
            GotoXY(13+4*(TurnCol[iT-1]-1),i);
            printf("   ");
        }
        for(i=0;i <Game.Rows - flaggedRow ;i++){
                GotoXY(13+4*(TurnCol[iT-1]-1),4+(i-1)*2);
                i>0?printf("   "):1;
                GotoXY(13+4*(TurnCol[iT-1]-1),4+i*2);
                SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FORMATDLGORD31);
                iT%2==0?printf(" O "):printf(" X ");
                SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FORMATDLGORD31);
            //}
            Sleep(20); /// Delays the output with *ms giving the animation effect
        }
        GotoXY(0, 4+ 2 +Game.Rows *2 + 4);
        printf("\n\n");
    }else
        printf("\n\n\n");

    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FORMATDLGORD31);
    start_turn();
}
void start_turn(){  /// reads column input from user in game
    int input;
    if( check_end())
        start_game();
    printf("\nUndo [z], Redo [r],Save [s], Exit [e]\n\n\t");
    if(iT%2 != 0 && Game.GameMode == 1){
        AI_main();
    }else if(iT%2 != 0){
        SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_GREEN);
        printf("Player 2 Turn : ");
    }else{
        SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN);
        printf("Player 1 Turn : ");
    }
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FORMATDLGORD31);

    char choice[20]={0};
    scanf("%s",&choice);
    input=atoi(choice); /// adds the ability to filter any kind of input
    if( choice[1]==0 && ( choice[0] == 's' || choice[0] == 'S')){
        save();
    }else if( choice[1]==0 && (choice[0] == 'e')){
        printf("Are you sure ? Yes[y], No[n] : ");
        char choice[20];
        scanf("\n%s",&choice);
        choice[0]=='y'||choice[0]=='Y'?start_game():print_game(-1);
    }else if( choice[1]==0 && (choice[0] == 'z' || choice[0] == 'Z')){
        if(!undo())
            print_game(3);
        else if (Game.GameMode == 1){
            undo();
            print_game(-1);
        }else
            print_game(-1);
    }else if( choice[1]==0 && (choice[0] == 'r' || choice[0] == 'R')){
        if(!redo())
            print_game(4);
        else if (Game.GameMode == 1){
            redo();
            print_game(-1);
        }else
            print_game(-1);
    }else if((int)input!=input || input>Game.Cols || input<1){
        print_game(1);
    }else{
        input--;
        if(insert((int)input))
            print_game(0);
        else
            print_game(2);
    }
}
int insert(int input){ ///inserts the column input from start_turn in the array
    int i,limit=true;
    for(i=0;i<Game.Rows;i++){
        if(gameArr[i][input]!=0)  /// skips any full fields
            continue;
        else{
            gameArr[i][input]=iT%2==0?1:2;
            TurnCol[iT]=input;
            flaggedRow=i;
            if(iT<nT)
                nT=iT;
            iT++;
            nT++;
            limit=false;
            break;
        }
    }
    if(!limit){
        return 1;
    }else{
        return 0;
    }
}
int undo(){
    int move,i;
    if(iT<1 || TurnCol[iT-1] < 0){ /// prevent undo from first game
        return false;
    }else{
        move=TurnCol[iT-1];
        for(i=0;i<=Game.Rows;i++){
            if(gameArr[i][move]!=0)
                continue;
            else{
                gameArr[i-1][move]=0;
                iT--;
                break;
            }
        }
        flaggedRow = i;
        return true;
    }
}
int redo(){
    int move,i;
    if(iT<nT){
        move=TurnCol[iT];
        for(i=0;i<Game.Rows;i++){
            if(gameArr[i][move]!=0)
                continue;
            else{
                gameArr[i][move]=iT%2==0?1:2;
                iT++;
                break;
            }
        }
        flaggedRow=i;
        return true;
    }else{
        return false;
    }
}
void save(){
    char sname[40]={0};

    int toggle=true;

    while (getchar() != '\n'); /// flush any \n

    while(toggle){ /// loop until the input is correct
        toggle=false;
        printf("\nEnter Save's Name : ");

        fgets(sname,sizeof sname, stdin);
        int i,stop=false;
        for(i=0;sname[i]!=0 && !stop;i++){  /// checks if the string contains anything other than letters
            if(sname[i] == ' ' || sname[i] == '\t' )
                stop=true;
        }
        sname[i-1]='\0';    /// Removes \n taken from fgets
        if(stop || sname[0]==0 || sname[0]=='\n'){
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY | FOREGROUND_RED);
            printf("\n Spaces aren't allowed ! Please try again");
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FORMATDLGORD31);
            toggle=true;
            continue;
        }
        strcat(sname,".bin");

        /// check if the file exists
        FILE *file;
        if (file = fopen(sname, "r"))
        {
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FORMATDLGORD30);
            printf("\nSave File with the same name Already Exists, try again \n");
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FORMATDLGORD31);
            toggle=true;
            continue;

        }else{
            fclose(file);
        }
    }

    int i=0,i2;

    FILE*saveFile=fopen(sname,"wb");
    int temp,tempI,rows=Game.Rows,cols=Game.Cols;
    fprintf(saveFile,"%c",(cols<<2 | Game.GameMode));
    for(i=0;i<rows;){
        tempI=i;
        for(i2=0;i2<cols;i2++){
            i=tempI;
            if(i<rows){
                temp=(gameArr[i][i2]) ;
                i++;
            }else{
                temp = 3;
            }
            if(i<rows){
                temp |=(gameArr[i][i2]<<2) ;
                i++;
            }else{
                temp |=3<<2 ;
            }
            if(i<rows){
                temp |=(gameArr[i][i2]<<4) ;
                i++;
            }else{
                temp |=3<<4 ;
            }
            if(i<rows){
                temp |=(gameArr[i][i2]<<6) ;
                i++;
            }else{
                temp |=3<<6 ;
            }
            fprintf(saveFile,"%c",temp);
//            fwrite(temp,sizeof temp , 1, saveFile);
        }
    }
    fclose(saveFile);

    printf("\n\nGame has been Save Successfully with the name %s!\n\n\nDo you want to continue playing? Yes[y], No[n]",sname);
    char choice[20];
    scanf("\n%s",&choice);
    if(choice[0]=='Y' || choice[0] == 'y')
        print_game(-1);
    else
        start_game();
}
void load(){

    char sname[40]={0};
    int toggle=true,i,i2;
    while (getchar() != '\n'); /// flush any \n

    while(toggle){ /// loop until the input is correct
        toggle=false;
        printf("\nEnter Save's Name : ");

        fgets(sname,sizeof sname, stdin);
        int i,stop=false;
        for(i=0;sname[i]!=0 && !stop;i++){  /// checks if the string contains anything other than letters
            if(sname[i] == ' ' || sname[i] == '\t' )
                stop=true;
        }
        sname[i-1]='\0';    /// Removes \n taken from fgets
        if(stop || sname[0]==0 || sname[0]=='\n'){
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY | FOREGROUND_RED);
            printf("\n Spaces aren't allowed ! Please try again");
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FORMATDLGORD31);
            toggle=true;
            continue;
        }
    }
    strcat(sname,".bin");
    /// check if the file exists
    FILE *file;
    if (file = fopen(sname, "r"))
    {
        fclose(file);
    }else{
        SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FORMATDLGORD30);
        printf("\nSave File %s Doesn't Exist, Try again ? Yes[y], No[n] \n",sname);
        SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FORMATDLGORD31);
        char choice[20];
        scanf("\n%s",&choice);
        if(choice[0]=='Y' || choice[0] == 'y')
            load();
        else
            start_game();
    }

    clear_game();
    int error=false;
    loading=true;

    int temp,tempRows=0,tempCols=0,tempGmode,tempnT=0;
    char tempArr[50][50];

    if(!error){
        FILE*saveFile=fopen(sname,"rb");
        char buffer[100];
        unsigned long fileLen;

        ///Get file length
        fseek(saveFile, 0, SEEK_END);
        fileLen=ftell(saveFile);
        fseek(saveFile, 0, SEEK_SET);
        if(fileLen < 4)
            error=true;

        ///Read file contents into buffer(memory)
        fread(buffer, fileLen, 1, saveFile);
        fclose(saveFile);
        tempCols = buffer[0] >> 2;
        tempGmode = buffer[0] & 3;

        int buff_count = 1;
        int r1,r2,r3,r4;

        for(tempRows=0;!error && buff_count<fileLen;){
            temp=tempRows;
            for(i2=0;i2<tempCols;i2++){
                tempRows=temp;
                r1=buffer[buff_count] & 3;
                r2=buffer[buff_count] >>2 & 3;
                r3=buffer[buff_count] >>4 & 3;
                r4=buffer[buff_count] >>6 & 3;
                if(r1 != 3){
                    tempArr[tempRows][i2]=r1;
                    tempRows++;
                    if(r1 != 0 )
                        tempnT++;
                }
                if(r2 != 3){
                    tempArr[tempRows][i2]=r2;
                    tempRows++;
                    if(r2 != 0 )
                        tempnT++;
                }
                if(r3 != 3){
                    tempArr[tempRows][i2]=r3;
                    tempRows++;
                    if(r3 != 0 )
                        tempnT++;
                }
                if(r4 != 3){
                    tempArr[tempRows][i2]=r4;
                    tempRows++;
                    if(r4 != 0 )
                        tempnT++;
                }
                buff_count++;
            }
        }
        if(i2 < tempCols-1)
            error=true;
    }



        /// Check if the file has been edited !!
        for(i=0;i<tempCols && !error;i++){
            for(i2=0;i2<tempRows && !error;i2++){
                if(i2 > 0 && tempArr[i2][i] != 0 && tempArr[i2-1][i] == 0)
                    error=true;
            }
        }
        /// copy game from temp array to game array after error check
        for(i=0;i<tempRows && !error;i++){
            for(i2=0;i2<tempCols;i2++){
                gameArr[i][i2]=tempArr[i][i2];
            }
        }
        if(error){
            loading = false;
            clear_game();
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FORMATDLGORD30);
            printf("\n\nLoad Error, File has been modified !\nDo you want to retry ? Yes[y], No[n] : ");
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FORMATDLGORD31);
            char choice[20];
            scanf("\n%s",&choice);
            choice[0]=='y'||choice[0]=='Y'?load():start_game();
        }else{
            loading = false;
            Game.GameMode=tempGmode;
            Game.Cols=tempCols;
            Game.Rows=tempRows;
            if(tempnT % 2 == 0){
                TurnCol[1]=-1;
                iT=2;nT=2;
            }else{
                TurnCol[2]=-1;
                iT=3;nT=3;
            }
            print_game(-1);
        }
}

void check_score(){
    int ScoreA=0;
    int ScoreB=0;
    int i,i2,i3;
    int current = 1;
    for(i3=0;i3<2;i3++){
        for(i=0;i < Game.Rows ; i++){
            for(i2 =0 ; i2 < Game.Cols ; i2++){
                if( gameArr[i][i2] != current )
                    continue;
                if(Game.Cols - i2 >= 4 &&  gameArr[i][i2+1] == current && gameArr[i][i2+2] == current && gameArr[i][i2+3] == current ){
                    current==1?ScoreA++:ScoreB++;
                }
                if( Game.Rows - i >= 4  && gameArr[i+1][i2] == current  && gameArr[i+2][i2]== current && gameArr[i+3][i2] == current){
                    current==1?ScoreA++:ScoreB++;
                }
                if( Game.Rows - i >= 4 && Game.Cols - i2 >= 4 && gameArr[i+1][i2+1] == current && gameArr[i+2][i2+2] == current && gameArr[i+3][i2+3] == current){
                    current==1?ScoreA++:ScoreB++;
                }
                if( i >= 3 && Game.Cols - i2 >= 4 && gameArr[i-1][i2+1] == current && gameArr[i-2][i2+2] == current && gameArr[i-3][i2+3] == current){
                    current==1?ScoreA++:ScoreB++;
                }
            }
        }
        current = 2;
    }
    Game.scoreA = ScoreA;
    Game.scoreB = ScoreB;
}

void AI_main(){
    Game.GameDifficulty = 4;
    struct AI minXmaxO={0,9999,-9999,-999,0,-9999,-9999,-999};
    int i;
    for( i = 0 ; i < Game.Cols ; i++){
         int row = 0 , valid = false;
        int currScoreO = 0;
        while( row < Game.Rows && !valid){
            if(gameArr[row][i] == 0)
                valid=true;
            else
                 row++;
        }
        if(!valid)
            continue;

        gameArr[row][i] = 2;
        currScoreO = check_score_AI(row,i,2);
        AI_check(0, &minXmaxO , Game.GameDifficulty - 1 , currScoreO ,0);

        if( minXmaxO.scoreX > minXmaxO.curr_cell_scoreX ){
            minXmaxO.col = i;
            minXmaxO.scoreO = minXmaxO.curr_cell_scoreO;
            minXmaxO.scoreX = minXmaxO.curr_cell_scoreX;
            minXmaxO.level = minXmaxO.curr_cell_level;
        }else if(minXmaxO.scoreX == minXmaxO.curr_cell_scoreX && minXmaxO.level < minXmaxO.curr_cell_level){
            minXmaxO.col = i;
            minXmaxO.scoreO = minXmaxO.curr_cell_scoreO;
            minXmaxO.scoreX = minXmaxO.curr_cell_scoreX;
            minXmaxO.level = minXmaxO.curr_cell_level;
        }else if (minXmaxO.scoreX == 0  && minXmaxO.curr_cell_scoreX == 0 && minXmaxO.scoreO == minXmaxO.curr_cell_scoreO){
            if(abs(i - Game.Cols/2) < abs(minXmaxO.col - Game.Cols/2)){
                minXmaxO.col = i;
                minXmaxO.scoreO = minXmaxO.curr_cell_scoreO;
                minXmaxO.scoreX = minXmaxO.curr_cell_scoreX;
                minXmaxO.level = minXmaxO.curr_cell_level;
            }
        }else if(minXmaxO.scoreX == minXmaxO.curr_cell_scoreX && minXmaxO.scoreO < minXmaxO.curr_cell_scoreO){
            minXmaxO.col = i;
            minXmaxO.scoreO = minXmaxO.curr_cell_scoreO;
            minXmaxO.scoreX = minXmaxO.curr_cell_scoreX;
            minXmaxO.level = minXmaxO.curr_cell_level;
        }
        gameArr[row][i] = 0;
        minXmaxO.curr_cell_level = 999;
        minXmaxO.curr_cell_scoreO = -99999;
        minXmaxO.curr_cell_scoreX = -99999;
    }
    insert(minXmaxO.col);
    print_game(0);
}
void AI_check(int cellCol,struct AI* minXmaxO,int difficulty, int currScoreO, int currScoreX){
    int row=0,valid=false;
    int TempScoreX = currScoreX;
    int TempScoreO = currScoreO;
    if(cellCol > Game.Cols)
        return ;
    /// Check if col has empty cell
    while( row < Game.Rows && !valid){
        if(gameArr[row][cellCol] == 0)
            valid=true;
        else
             row++;
    }
    if(difficulty > 2 && valid){
        if( difficulty % 2 == 0){
            gameArr[row][cellCol]=2;
            currScoreO += check_score_AI(row,cellCol,2);
            AI_check(0, minXmaxO , difficulty - 1 , currScoreO ,currScoreX);
            gameArr[row][cellCol]=0;
        }else{
            gameArr[row][cellCol] = 1;
            currScoreX += check_score_AI(row,cellCol,1);
            if( currScoreX > TempScoreX && difficulty > minXmaxO->curr_cell_level )
                minXmaxO->curr_cell_level = difficulty ;
            AI_check(0, minXmaxO , difficulty - 1 , currScoreO ,currScoreX);
            gameArr[row][cellCol] = 0 ;
        }
    }else if(valid){
        TempScoreX += check_score_AI(row,cellCol,1);
        TempScoreO += check_score_AI(row,cellCol,2);
        if( currScoreX < TempScoreX && difficulty > minXmaxO->curr_cell_level )
            minXmaxO->curr_cell_level = difficulty ;
        if(minXmaxO->curr_cell_scoreX < TempScoreX){
            minXmaxO->curr_cell_scoreX = TempScoreX;
        }
        if(minXmaxO->curr_cell_scoreO < TempScoreO){
            minXmaxO->curr_cell_scoreO = currScoreO;
        }

    }
    AI_check(cellCol +1, minXmaxO , difficulty , currScoreO , currScoreX);
}

int check_score_AI(int row, int col, int current){
    int TempScoreA = 0;
    int TempScoreB = 0;

    /// Horizontal
    if(Game.Cols-col >= 3 &&  gameArr[row][col+1] == current && gameArr[row][col+2] == current && gameArr[row][col+3] == current ){
        if(current==1)
            TempScoreA++;
        else
            TempScoreB++;
    }

    if(col >= 1 && Game.Cols-col >= 1 &&  gameArr[row][col-1] == current && gameArr[row][col+1] == current && gameArr[row][col+2] == current ){
        if(current==1)
            TempScoreA++;
        else
            TempScoreB++;
    }

    if(col >= 0 && Game.Cols-col >= 2 &&  gameArr[row][col-2] == current && gameArr[row][col-1] == current && gameArr[row][col+1] == current ){
        if(current==1)
            TempScoreA++;
        else
            TempScoreB++;
    }
    if(col >= 2 &&  gameArr[row][col-1] == current && gameArr[row][col-2] == current && gameArr[row][col-3] == current ){
        if(current==1)
            TempScoreA++;
        else
            TempScoreB++;
    }
    ///Horizontal

    ///Vertical
    if( row >= 3  && gameArr[row-1][col] == current && gameArr[row-2][col]== current && gameArr[row-3][col] == current){
        if(current==1)
            TempScoreA++;
        else
            TempScoreB++;
    }
    ///Vertical

    ///Diagonal
    if( Game.Rows - row >= 4 && Game.Cols - col >= 4 && gameArr[row+1][col+1] == current && gameArr[row+2][col+2] == current && gameArr[row+3][col+3] == current){
        if(current==1)
            TempScoreA++;
        else
            TempScoreB++;
    }
    if( Game.Rows - row >= 3 && Game.Cols - col >= 3 && gameArr[row+1][col+1] == current && gameArr[row+2][col+2] == current && gameArr[row-1][col-1] == current){
        if(current==1)
            TempScoreA++;
        else
            TempScoreB++;
    }
    if( Game.Rows - row >= 2 && Game.Cols - col >= 2 && gameArr[row+1][col+1] == current && gameArr[row-1][col-1] == current && gameArr[row-2][col-2] == current){
        if(current==1)
            TempScoreA++;
        else
            TempScoreB++;
    }

    if( row >= 3 && Game.Cols - col >= 4 && gameArr[row-1][col+1] == current && gameArr[row-2][col+2] == current && gameArr[row-3][col+3] == current){
        if(current==1)
            TempScoreA++;
        else
            TempScoreB++;
    }
    if( row >= 2 && Game.Cols - col >= 3 && gameArr[row-1][col+1] == current && gameArr[row-2][col+2] == current && gameArr[row+1][col-1] == current){
        if(current==1)
            TempScoreA++;
        else
            TempScoreB++;
    }
    if( row >= 1 && Game.Cols - col >= 2 && gameArr[row-1][col+1] == current && gameArr[row+1][col-1] == current && gameArr[row+2][col-2] == current){
        if(current==1)
            TempScoreA++;
        else
            TempScoreB++;
    }

    if( row >= 3 && col >= 3 && gameArr[row-1][col-1] == current && gameArr[row-2][col-2] == current && gameArr[row-3][col-3] == current){
        if(current==1)
            TempScoreA++;
        else
            TempScoreB++;
    }
    if( Game.Rows - row >= 4 && col >= 3 && gameArr[row+1][col-1] == current && gameArr[row+2][col-2] == current && gameArr[row+3][col-3] == current){
        if(current==1)
            TempScoreA++;
        else
            TempScoreB++;
    }
    ///Diagonal

    if(current == 1)
        return TempScoreA;
    else
        return TempScoreB;
}

int main()
{   HWND hWnd;
   SetConsoleTitle("Connect 4");
   hWnd = FindWindow(NULL, "Connect 4");
    HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD NewSBSize = GetLargestConsoleWindowSize(hOut);
    SMALL_RECT DisplayArea = {0, 0, 0, 0};

    SetConsoleScreenBufferSize(hOut, NewSBSize);

    DisplayArea.Right = NewSBSize.X - 1;
    DisplayArea.Bottom = NewSBSize.Y - 1;

    SetConsoleWindowInfo(hOut, TRUE, &DisplayArea);

    ShowWindow(hWnd, SW_MAXIMIZE);
    system("color f");
    start_game();

    return 0;
}
